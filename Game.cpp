#include "Game.h"

Game::Game() : window(sf::VideoMode(800, 600), "Steel Tigers v.01")
{}

void Game::run(int frames_per_second)
{
	sf::Clock clock;
	sf::Time time_since_last_update = sf::Time::Zero;
	sf::Time time_per_frame = sf::seconds(1.f/frames_per_second);
    
	while(window.isOpen())
	{
		process_events();
		
		bool repaint = false;
		time_since_last_update += clock.restart();
		while(time_since_last_update > time_per_frame)
		{
			time_since_last_update -= time_per_frame;
			repaint = true;
			update(time_per_frame);	
		}

		if(repaint)
			render();
	}
}

void Game::process_events()
{
	sf::Event event;
	while(window.pollEvent(event))
		{
			switch(event.type)
			{
				case sf::Event::Closed:
					window.close();
					break;

				case sf::Event::KeyPressed:
				{
					switch(event.key.code)
					{
						case sf::Keyboard::Escape:
							window.close();
						default: break;
					}
				}break;

				default: break;
			}
		}
}

void Game::render()
{
	window.clear();
	window.display();
}

void Game::update(sf::Time delta_time)
{

}