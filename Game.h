#ifndef GAME_H_
#define GAME_H_

#include <SFML/Graphics.hpp>

class Game
{
public:
    Game(const Game&) = delete;
    Game& operator=(const Game&) = delete;
    Game();
    void run(int frames_per_second);
    void process_events();
    void update(sf::Time delta_time);
    void render();
private:
    sf::RenderWindow window;
};

#endif //GAME_H_