#include <iostream>
#include "Game.h"

int main()
{
	std::cout << "Steel Tigers v0.1" << std::endl;

	Game g;
	g.run();
	return 0;
}